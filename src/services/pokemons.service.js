export class Pokemons{

    static sortByName(){
        return (a, b) => (a.name > b.name) ? 1 : -1
    }

}