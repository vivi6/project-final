import { html, LitElement } from "lit";

export class HeaderUI extends LitElement {
  

    render() {
        return html`
        <div id="container-header">
            <img id="img-logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1280px-International_Pok%C3%A9mon_logo.svg.png" alt="logo pokemon">
            <nav id="nav-header">
                <a class="a-nav" href="/pokedex">Pokedex</a>
                <a class="a-nav" href="/">Pokemons</a>
            </nav>
        </div>
        
        `;
    }

    createRenderRoot(){
        return this;
    }

}

customElements.define('header-ui', HeaderUI);