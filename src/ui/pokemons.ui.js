import { html, LitElement } from "lit";

export class PokemonsUI extends LitElement {

    static get properties() {
        return {
            pokemons: {
                type: Array
            }
        }
    }
  

    render() {
        return html`
        <section id="container-pokemons">
            <h1>Todos los pokemons</h1>
            <ul id="pokemons">
            ${this.pokemons && this.pokemons?.map((pokemon) =>
                html`
                <li class="pokemon">
                    <p id="pokemon_${pokemon.name}">${pokemon.name}</p>
                </li>`
                )}
            </ul>
        </section>
        `;
    }

    createRenderRoot(){
        return this;
    }

}

customElements.define('pokemons-ui', PokemonsUI);