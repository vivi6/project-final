import './../components/pokemon.component';
import '../ui/header.ui';

export class PokemonsPage extends HTMLElement{

    constructor(){
        super();
    }

    connectedCallback(){
        this.render()
    }

    render(){
        this.innerHTML=`
           <pokemon-component></pokemon-component>
        `;
    }
}

customElements.define('pokemons-page',PokemonsPage)