import { state } from '../states/state';
import { snapshot } from 'valtio/vanilla';
import { ProfilePokemonComponent } from "../components/profile-pokemon.component";
import { subscribe } from 'valtio/vanilla';
import '../ui/header.ui';
import {RemovePokemonUseCase} from "../usecases/remove-pokemon.usecase";

export class PokedexPage extends HTMLElement{

    constructor(){
        super();
    }

    connectedCallback(){
        this.render()
        subscribe(state, () =>{
            this.render()
        })
    }

    
    render() {
        var snap = snapshot(state.pokemons);
        document.addEventListener("remove:pokemon", (e) => {
            const useCase = new RemovePokemonUseCase();
            useCase.execute(e.detail.name,snap);
        });

        if(snap.length != 0 ){
            this.innerHTML = `
            <h1 class="title">Pokemons en mi Pokedex</h1>
            <section id="container-profile-all-pokemons">
                ${snap.map((namePokemon) =>
                `<div class="container-section">
                    <profile-pokemon-component btnRemove=${true} namePokemon=${namePokemon}></profile-pokemon-component>
                </div>`
                ).join('')}
            </section>`

        }else{
            this.innerHTML = `
            <section class="container-section-pokedex-empty">
                <p class="title">No tienes pokemons en tu pokedex</p>
                <img id="sad-pikachu" src="https://www.nintenderos.com/wp-content/uploads/2021/04/pikachu-pokemon.jpg" alt="pikachu triste">
            </section>`
        }

        
        
    }

}

customElements.define('pokedex-page',PokedexPage)