import axios from "axios";

const BASE_URL = 'https://pokeapi.co/api/v2/';
const url = 'https://pokeapi.co/api/v2/pokemon/';

export class PokemonRepository {


    async getAllPokemons() {
        return await (
            await axios.get(BASE_URL + 'pokemon?limit=1126')
        ).data;
    }

    async getProfilePokemon(name) {
        return await (
            await axios.get(url + name)
        ).data;
    }

}