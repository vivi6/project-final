import { html, LitElement } from "lit";
import {ProfilePokemonUseCase} from '../usecases/profile-pokemon.usecase';


export class ProfilePokemonComponent extends LitElement {

    static get properties() {
        return {
            namePokemon: {
                type: String,
            },
            profilePokemon: {
                type: Object,
            },
            btnRemove: {
                type: Boolean,
            },
            visible: {
                type: Boolean,
            }
        }
    }

    async connectedCallback() {
        super.connectedCallback();
    }
    
    async update(changedProperties) {
        super.update(changedProperties);
        const profilePokemonUseCase = new ProfilePokemonUseCase();
        this.profilePokemon = await profilePokemonUseCase.execute(this.namePokemon) //perfil pokemon por la api
    }
    
    async shouldUpdate(changedProperties) {
        return changedProperties.has('namePokemon');
    }

    addPokemon(namePokemon){
        const event = new CustomEvent("add:pokemon", {
            detail: {
                name: namePokemon
            },
            bubbles:true,
            composed: true,
        });
        this.dispatchEvent(event);
    }
  
    removePokemon(namePokemon){
        const event = new CustomEvent("remove:pokemon", {
            detail: {
                name: namePokemon,
            },
            bubbles:true,
            composed: true,
        });
        this.dispatchEvent(event);
    }

    render() {
        return html`
                    <article class="container-profile-pokemon" id="profile_pokemon_${this.namePokemon}">
                        <figure class="container-figure">
                            <h1>${this.namePokemon}</h1>
                            <img id="img-pokemon-frontal" src="${this.profilePokemon?.sprites.front_default}" alt="imagen pokemon frontal">
                            <figcaption class="highlight__caption">
                                <p class="atr-pokemon">Experiencia base: ${this.profilePokemon?.base_experience}</p>
                                <p class="atr-pokemon">Altura: ${this.profilePokemon?.height} m</p>
                                <p class="atr-pokemon">Peso: ${this.profilePokemon?.weight} kg</p>
                                <p class="pokemon-type">Tipo: ${this.profilePokemon?.types.map( (tipo) =>
                                        html`<p class="type">${tipo.type.name}</p>`
                                    )}
                                </p>
                            </figcaption>
                        </figure>
                        <footer>
                            ${this.btnRemove ? html ` <button id="removePokemon" @click="${()=>this.removePokemon(this.namePokemon)}" >Eliminar</button>` : ""}
                            ${this.visible ? html ` <button id="addPokemon" @click="${() => this.addPokemon(this.namePokemon)}" >Añadir a Pokedex</button>` : "" }
                        </footer>
                    </<article>`
                   
        ;
    }

    createRenderRoot(){
        return this;
    }

}

customElements.define('profile-pokemon-component', ProfilePokemonComponent);