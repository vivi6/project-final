import { html, LitElement } from "lit";
import './../ui/pokemons.ui';


export class SearchComponent extends LitElement {
    
    async connectedCallback() {
        super.connectedCallback();
    }
    
    searchPokemons(){
        const namePokemon = document.getElementById("name-pokemon").value
        const event = new CustomEvent("search:pokemon", {
            detail: {
                name: namePokemon,
            },
            bubbles:true,
            composed: true,
        });
        this.dispatchEvent(event);
        document.getElementById("name-pokemon").value=""
    }


    render() {

        return html`
            <div id="container-search">
                <label id="label-pokemon">Search pokemon:</label>
                <input type="text" id="name-pokemon">
                <button @click="${this.searchPokemons}" id="searchPokemons">Buscar</button>
            </div>
        `;
         
    }

    createRenderRoot(){
        return this;
    }
}


customElements.define('search-component', SearchComponent);
