import { html, LitElement } from "lit";
import { AllPokemonsUseCase } from "../usecases/all-pokemon.usecase";
import { SearchPokemonUseCase } from "../usecases/search-pokemon.usecase";
import { AddPokemonUseCase } from "../../src/usecases/add-pokemon.usecase"
import { SortPokemonUseCase } from "../../src/usecases/sort-pokemon.usecase";
import { ProfilePokemonComponent } from "../components/profile-pokemon.component";
import { SearchComponent } from '../components/search-component';
import './../ui/pokemons.ui';
import { subscribe } from 'valtio/vanilla'
import { state } from '../states/state';

export class PokemonComponent extends LitElement {
    
    static get properties() {
        return {
            pokemons: {
                type: Array,
                state: true
            },
            pokemon: {
                type: Object,
            },
            profilePokemon: {
                type: Object,
            },
            namePokemon: {
                type: String
            },
            visible: {
                type: Boolean
            }
        }
    }

    async connectedCallback() {
        super.connectedCallback();
        const allPokemonsUseCase = new AllPokemonsUseCase();
        this.pokemons = await allPokemonsUseCase.execute();
        this.visible=true
        subscribe(state, () =>{
            this.render()
        })
    }
    

    async sortPokemons(){
        const sortPokemonUseCase = new SortPokemonUseCase();
        const sortedPokemons = await sortPokemonUseCase.execute(this.pokemon)
        this.pokemons = sortedPokemons
    }


    render() {
        
        document.addEventListener("add:pokemon", (e) => {
            const usecase = new AddPokemonUseCase()
            usecase.execute(e.detail.name)
        });

        document.addEventListener("search:pokemon", (e) => {
            const searchPokemonUseCase = new SearchPokemonUseCase();
            this.pokemon = searchPokemonUseCase.execute(e.detail.name,this.pokemons); 
        });

        return html`
        <nav id="nav-secondary">
            <button @click="${this.sortPokemons}" id="sortPokemons">Ordenar A-Z</button>
            <search-component></search-component>
        </nav>
        
        ${ this.pokemon?.length > 0
            ? html `<section id="container-profile-all-pokemons">
            ${this.pokemon.map( (p)=>
                html `<div class="container-section">
                            <profile-pokemon-component .visible=${this.visible} .namePokemon="${p.name}"></profile-pokemon-component>
                        </div>
                    </section>`
            )}`
                
            : html `<pokemons-ui .pokemons="${this.pokemons}"></pokemons-ui>`}
        `;
         
    }

    createRenderRoot(){
        return this;
    }
}


customElements.define('pokemon-component', PokemonComponent);
