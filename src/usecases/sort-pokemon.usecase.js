import { PokemonRepository } from '../repositories/pokemon.repository';
import {Pokemons} from '../services/pokemons.service';

export class SortPokemonUseCase {

    async execute(){
        const repository = new PokemonRepository()
        const listPokemons = await repository.getAllPokemons()
        return  listPokemons.results.sort(Pokemons.sortByName())
        
    }


}