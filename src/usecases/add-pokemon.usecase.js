import { state } from "../states/state";
import { snapshot } from 'valtio/vanilla';

export class AddPokemonUseCase{

    execute(name){
        var snap = snapshot(state.pokemons);
        const filter = snap.filter(namePokemon => namePokemon === name);
        if (filter.length === 0) {
            state.pokemons.push(name) 
            return true;
        }
        return false
    }
}