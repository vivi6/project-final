import { PokemonRepository } from "../repositories/pokemon.repository";

export class AllPokemonsUseCase {

    async execute() {
        const repository = new PokemonRepository();
        const pokemons = await repository.getAllPokemons();
        return pokemons.results;
    }


}