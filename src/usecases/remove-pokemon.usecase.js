import { state } from "../states/state";
import { snapshot } from 'valtio/vanilla';

export class RemovePokemonUseCase{

    execute(name,snap){
        const removedPokemon = snap.filter(namePokemon => namePokemon !== name);
        state.pokemons = removedPokemon
    }
}