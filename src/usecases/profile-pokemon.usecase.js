import { PokemonRepository } from '../repositories/pokemon.repository';

export class ProfilePokemonUseCase{

    async execute(name_pokemon){
        const repository = new PokemonRepository();
        const pokemons = await repository.getProfilePokemon(name_pokemon);
        return pokemons;
    }

}