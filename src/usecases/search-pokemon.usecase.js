export class SearchPokemonUseCase{

    execute(namePokemon, pokemons){
        var pokemon;
        if( !!namePokemon ){
            pokemon = pokemons.filter( (pokemon) => 
                pokemon.name.includes(namePokemon.toLowerCase())
            )
        }
        return pokemon;
    }

}