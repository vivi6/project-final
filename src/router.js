import {Router} from '@vaadin/router';
import './pages/pokemons.page';
import './pages/pokedex.page';
const outlet = document.querySelector('#outlet');
const router = new Router(outlet);

router.setRoutes([
    {path: '/', component: 'pokemons-page'},
    {path: '/pokedex', component: 'pokedex-page'},
    {path: '(.*)', component: '/'},
])