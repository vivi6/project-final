/// <reference types="Cypress" />

beforeEach( () => {
    cy.intercept('GET', 'https://pokeapi.co/api/v2/pokemon?limit=1126', {fixture: './../../fixtures/pokemons.json'})
})

it('user have not pokemons on pokedex', () =>{
    cy.visit('/pokedex');
    cy.get('.title').should('exist')
    cy.get('#profile_pokemon_zygarde-complete').should('not.exist')
})

// it('user view the pokemons in your pokedex', () =>{
//     cy.visit('/');
//     cy.get('#name-pokemon').type('zygarde');
//     cy.get('#searchPokemons').click();
//     cy.get('#profile_pokemon_zygarde-50 > footer > #addPokemon').click();
//     cy.visit('/pokedex');
//     cy.get('#profile_pokemon_zygarde-50').should('exist')
// })

// it('user deleted a pokemon in your pokedex', () =>{
//     cy.visit('/');
//     cy.get('#name-pokemon').type('zygarde');
//     cy.get('#searchPokemons').click();
//     cy.get('#profile_pokemon_zygarde-50 > footer > #addPokemon').click();
//     cy.visit('/pokedex');
//     cy.get('#profile_pokemon_zygarde-50').should('exist')
//     cy.get('#removePokemon').click();
//     cy.get('#profile_pokemon_zygarde-50').should('not.exist')
// })