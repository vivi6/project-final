/// <reference types="Cypress" />

beforeEach( () => {
    cy.intercept('GET', 'https://pokeapi.co/api/v2/pokemon?limit=1126', {fixture: './../../fixtures/pokemons.json'})
})

it('user displays all pokemons in the page', () =>{
    cy.visit('/');
    cy.wait(0);
    cy.get('.pokemon').should('have.length.gt', 1)
    .then( (items) => {
        cy.get('.pokemon > p').should('have.length',items.length)
    })
})


it('user click button sort Pokemon on pokemons page', () =>{
    cy.visit('/');
    cy.get('#sortPokemons').click();
    cy.wait(0);
    cy.get('#pokemons > :nth-child(1) > p').should('have.text','abomasnow')
    
})

it('user click link to pokedex page from pokemons page', () =>{
    cy.visit('/');
    cy.get('[href="/pokedex"]').click().then( () => {
        cy.visit('/pokedex')
    })
})


it('user click button search with "zygarde" text', () =>{
    cy.visit('/');
    cy.get('#name-pokemon').type('zygarde');
    cy.get('#searchPokemons').click();
    cy.get('#profile_pokemon_zygarde-50').should('exist')
    cy.get('#profile_pokemon_zygarde-50').should('exist')
    cy.get('#profile_pokemon_zygarde-10-power-construct').should('exist')
    cy.get('#profile_pokemon_zygarde-50-power-construct').should('exist')
    cy.get('#profile_pokemon_zygarde-complete').should('exist')
    cy.get('#profile_pokemon_pikachu').should('not.exist')
})



