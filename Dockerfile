FROM node:13.10.1-alpine3.11 as builder
RUN mkdir /spa-starter
WORKDIR /spa-starter
COPY . .
RUN npm ci
RUN npm run build -- --output-path=dist
# RUN npm run build -- --output-path=dist
FROM nginx:1.13.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /spa-starter/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
