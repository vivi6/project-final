import { SearchPokemonUseCase } from "../../src/usecases/search-pokemon.usecase"
import POKEMONS from '../../fixtures/pokemons.json'

describe("Search Pokemon", () => {


    it('pokemon found', () => {
        let namePokemon = "pikachu"
        const searchPokemon = new SearchPokemonUseCase();
        const pokemon = searchPokemon.execute(namePokemon, POKEMONS.results);
        expect(pokemon).toHaveLength(17);
    })

    it('pokemon found with uppercase', () => {
        let namePokemon = "PIKACHU"
        const searchPokemon = new SearchPokemonUseCase();
        const pokemon = searchPokemon.execute(namePokemon, POKEMONS.results);
        expect(pokemon).toHaveLength(17);
    })

    it('pokemon with name not found', () => {
        let namePokemon = "vivianita"
        const searchPokemon = new SearchPokemonUseCase();
        const pokemon = searchPokemon.execute(namePokemon, POKEMONS.results);
        expect(pokemon).toHaveLength(0);
    })

    it('field name empty', () => {
        let namePokemon = ""
        const searchPokemon = new SearchPokemonUseCase();
        const pokemon = searchPokemon.execute(namePokemon, POKEMONS.results);
        expect(pokemon).toBeUndefined();
    })

   

    
})