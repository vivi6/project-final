import { PokemonRepository } from "../../src/repositories/pokemon.repository"
import { ProfilePokemonUseCase } from "../../src/usecases/profile-pokemon.usecase"
import PROFILE_BULBASAUR from '../../fixtures/profile-bulbasaur.json'

jest.mock('../../src/repositories/pokemon.repository');
describe("Profile ", () => {

    beforeEach(() => {
        PokemonRepository.mockClear();
    })

    it('profile pokemon found correctly', async () => {
        
        PokemonRepository.mockImplementation(() => {
            return {
                getProfilePokemon: () => {
                    return PROFILE_BULBASAUR; 
                }
            }
        })
        
        const useCase = new ProfilePokemonUseCase();
        const pokemon = await useCase.execute('bulbasaur');

        expect(pokemon.name).toBe('bulbasaur');
        // expect(pokemon.weight).toBe(69);
        // expect(pokemon.height).toBe(7);
        // expect(pokemon.id).toBe(1);
        // expect(pokemon.order).toBe(1);
        // expect(pokemon.sprites).toBe("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png");
        // expect(profilePokemon).toHaveLength(PROFILE_BULBASAUR.length);

    })


})