import { RemovePokemonUseCase } from "../../src/usecases/remove-pokemon.usecase"
import { snapshot } from 'valtio/vanilla';
import { state } from "../../src/states/state";
describe("Remove pokemon to pokedex", () => {
    it('pokemon removed correctly in the pokedex', () => {

        state.pokemon = ["pikachu"]
        var snap = snapshot(state.pokemons);
        const useCase = new RemovePokemonUseCase();
        useCase.execute("pikachu", snap);
        expect(snap).toEqual([]);
    })

    it('pokemon charmander removed of array state', () => {
        state.pokemons = ["pikachu","charmander"]
        var capture = snapshot(state.pokemons)
        const useCase = new RemovePokemonUseCase();
        useCase.execute("charmander", capture);
        var snap = snapshot(state.pokemons);
        expect(snap).toEqual(["pikachu"]);
    })

    
})