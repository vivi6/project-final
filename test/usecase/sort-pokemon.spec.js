import POKEMONS from '../../fixtures/pokemons.json'
import {SortPokemonUseCase} from '../../src/usecases/sort-pokemon.usecase'

describe( 'Sort pokemon alphabetically' , () => {
    
    it('sorted pokemon correctly from A to Z', async () => { 
        const usecase = new SortPokemonUseCase()
        const sortListPokemons = await usecase.execute()
        expect(sortListPokemons).toHaveLength(POKEMONS.results.length)
    })

    it('first pokemon should show is abomasnow', async () => {
        const usecase = new SortPokemonUseCase()
        const sortListPokemons = await usecase.execute()
        expect(sortListPokemons[0].name).toBe('abomasnow');
    })

    it('last pokemon should show is zygarde-complete', async () => {
        const usecase = new SortPokemonUseCase()
        const sortListPokemons = await usecase.execute()
        expect(sortListPokemons[1125].name).toBe('zygarde-complete');
    })

})