import { AddPokemonUseCase } from "../../src/usecases/add-pokemon.usecase"
import { state } from "../../src/states/state";
describe("Add pokemon to pokedex", () => {


    it('pokemon added correctly if not found in the pokedex', () => {
        state.pokemons = []
        const useCase = new AddPokemonUseCase();
        const  pokemon = useCase.execute("pikachu");
        expect(pokemon).toBeTruthy();
    })

    it('pokemon found in the pokedex ', () => {
        state.pokemon = ["pikachu"]
        const useCase = new AddPokemonUseCase();
        const  pokemon = useCase.execute("pikachu");
        expect(pokemon).toBeFalsy()
    })

    
})