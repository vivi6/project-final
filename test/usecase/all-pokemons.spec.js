import { PokemonRepository } from "../../src/repositories/pokemon.repository"
import {AllPokemonsUseCase} from "../../src/usecases/all-pokemon.usecase"
import POKEMONS from '../../fixtures/pokemons.json'

jest.mock('../../src/repositories/pokemon.repository');
describe("Search", () => {

    beforeEach(() => {
        PokemonRepository.mockClear();
    })

    it('should execute mock correctly', async () => {

        PokemonRepository.mockImplementation(() => {
            return {
                getAllPokemons: () => {
                    return POKEMONS; 
                }
            }
        })

        const useCase = new AllPokemonsUseCase();
        const pokemons = await useCase.execute();

        expect(pokemons).toHaveLength(POKEMONS.results.length);

    })

})